# Maratona de Programação

Uma escola será sede de uma maratona de programação, onde diversas equipes irão competir. A lista de alunos competidores em um arquivo do tipo csv. Modele um sistema que faça a leitura da lista e gere uma lista randômica de equipes.   

- Para a maratona desse ano, as equipes serão compostas por 3 alunos.

- Os alunos são organizados em equipes de forma completamente randômica. Um aluno não pode pertencer a duas equipes.

- Cada equipe recebe um id numérico sequencial para identificá-la de forma única.

- Caso haja um número irregular de alunos, a última equipe pode ficar com menos de 3 membros. 

- A lista de todas as equipes com os respectivos nomes deve ser exibida no console após a execução do programa.

